import Foundation

class Emoji
{
    let filename: String
    let nickname: String

    init(filename: String, nickname: String)
    {
        self.filename = filename
        self.nickname = nickname
    }
}
