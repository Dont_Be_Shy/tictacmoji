import UIKit


class GameVC: UIViewController
{
    @IBOutlet weak var gameBoard: UIStackView!
    @IBOutlet weak var square1: UIButton!
    @IBOutlet weak var square2: UIButton!
    @IBOutlet weak var square3: UIButton!
    @IBOutlet weak var square4: UIButton!
    @IBOutlet weak var square5: UIButton!
    @IBOutlet weak var square6: UIButton!
    @IBOutlet weak var square7: UIButton!
    @IBOutlet weak var square8: UIButton!
    @IBOutlet weak var square9: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    var allSquares = [UIButton()]
    
    var whoseTurn = "Player 1"
    let clickSound = AudioHelpers.getAudioPlayer("click", fileExtension: ".wav")
    let backgroundMusic = AudioHelpers.getAudioPlayer("peanutBackgroundMusic", fileExtension: ".mp3")
    
    var player1: Int?
    var player2: Int?
    var allEmojiCharacters: [Emoji?] = []
    
    override func viewDidLoad()
    {
        backgroundMusic?.player?.play()
        
        allSquares.append(square1)
        allSquares.append(square2)
        allSquares.append(square3)
        allSquares.append(square4)
        allSquares.append(square5)
        allSquares.append(square6)
        allSquares.append(square7)
        allSquares.append(square8)
        allSquares.append(square9)
        
        for s in allSquares
        {
            s.setTitle("", for: .normal)
        }
    }

    let characterVC = CharacterSelectVC()
    
    @IBAction func square1Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 1)
    }
    
    @IBAction func square2Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 2)
    }
    
    @IBAction func square3Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 3)
    }
    
    @IBAction func square4Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 4)
    }
    
    @IBAction func square5Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 5)
    }
    
    @IBAction func square6Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 6)
    }
    
    @IBAction func square7Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 7)
    }
    
    @IBAction func square8Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 8)
    }
    
    @IBAction func square9Tapped(_ sender: Any)
    {
        squareTapped(whichSquare: 9)
    }
    
    func squareTapped(whichSquare: Int)
    {
        clickSound?.player?.play()

        if whoseTurn == "Player 1"
        {
            allSquares[whichSquare].setImage(UIImage(named: allEmojiCharacters[player1!]!.filename), for: .normal)
            allSquares[whichSquare].setImage(UIImage(named: allEmojiCharacters[player1!]!.filename), for: .disabled)
        }
        else
        {
            allSquares[whichSquare].setImage(UIImage(named: allEmojiCharacters[player2!]!.filename), for: .normal)
            allSquares[whichSquare].setImage(UIImage(named: allEmojiCharacters[player2!]!.filename), for: .disabled)

        }
        
        allSquares[whichSquare].isEnabled = false
        
        if didSomeoneWin()
        {
            disableBoard()
            popupGameOverText()
        }
        else if boardIsFull()
        {
            resultLabel.text = "TIE GAME"
            popupGameOverText()
        }
        
        changeTurns()
    }

    func changeTurns()
    {
        if whoseTurn == "Player 1"
        {
            whoseTurn = "Player 2"
        }
        else
        {
            whoseTurn = "Player 1"
        }
    }
    
    func didSomeoneWin() -> Bool
    {
        var checkForWinner = checkCase(num1: 1, num2: 2, num3: 3)
        checkForWinner = checkForWinner || checkCase(num1: 4, num2: 5, num3: 6)
        checkForWinner = checkForWinner || checkCase(num1: 7, num2: 8, num3: 9)
        checkForWinner = checkForWinner || checkCase(num1: 1, num2: 4, num3: 7)
        checkForWinner = checkForWinner || checkCase(num1: 2, num2: 5, num3: 8)
        checkForWinner = checkForWinner || checkCase(num1: 3, num2: 6, num3: 9)
        checkForWinner = checkForWinner || checkCase(num1: 1, num2: 5, num3: 9)
        checkForWinner = checkForWinner || checkCase(num1: 3, num2: 5, num3: 7)
        
        return checkForWinner
    }
    
    func checkCase(num1: Int, num2: Int, num3: Int) -> Bool
    {
        if allSquares[num1].imageView?.image == allSquares[num2].imageView?.image &&
            allSquares[num1].imageView?.image == allSquares[num3].imageView?.image
        {
            if allSquares[num1].imageView?.image == UIImage(named: allEmojiCharacters[player1!]!.filename)
            {
                resultLabel.text = allEmojiCharacters[player1!]!.nickname + " won!" //"PLAYER 1 WON!"
                return true
            }
            else if allSquares[num1].imageView?.image == UIImage(named: allEmojiCharacters[player2!]!.filename)
                
            {
                resultLabel.text = allEmojiCharacters[player2!]!.nickname + " won!" //"PLAYER 2 WON!"
                return true
            }
        }
        
        return false
    }
    
    func disableBoard()
    {
        for s in allSquares
        {
            s.isEnabled = false
        }
    }
    
    func boardIsFull() -> Bool
    {
        for s in [square1, square2, square3, square4, square5, square6, square7, square8, square9]
        {
            // square is enabled when it's a ?
            // square is disabled when it's a fox or poop
            
            // if there are any ?'s, the board is not full -- return false
            
            if (s as! UIButton).isEnabled
            {
                return false
            }
        }
        
        return true
    }
    
    func popupGameOverText()
    {
        backgroundMusic?.player?.setVolume(0.0, fadeDuration: 2.0)
        gameBoard.isHidden = true
        
        Timer.scheduledTimer(timeInterval: 2,
                             target: self,
                             selector: #selector(goBackToMenu),
                     userInfo: nil,
                             repeats: false)
    }
    
    @objc func goBackToMenu()
    {
        dismiss(animated: true, completion: nil)
    }
}
