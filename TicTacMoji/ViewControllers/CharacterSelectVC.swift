import Foundation
import UIKit

class CharacterSelectVC: UIViewController
{
    @IBOutlet weak var emoji1: UIButton!
    @IBOutlet weak var emoji2: UIButton!
    @IBOutlet weak var emoji3: UIButton!
    @IBOutlet weak var emoji4: UIButton!
    @IBOutlet weak var emoji5: UIButton!
    @IBOutlet weak var emoji6: UIButton!
    @IBOutlet weak var emoji7: UIButton!
    @IBOutlet weak var emoji8: UIButton!
    @IBOutlet weak var emoji9: UIButton!
    @IBOutlet weak var emoji10: UIButton!
    @IBOutlet weak var emoji11: UIButton!
    @IBOutlet weak var emoji12: UIButton!
    @IBOutlet weak var emoji13: UIButton!
    @IBOutlet weak var emoji14: UIButton!
    @IBOutlet weak var emoji15: UIButton!
    @IBOutlet weak var emoji16: UIButton!
    @IBOutlet weak var emoji17: UIButton!
    @IBOutlet weak var emoji18: UIButton!
    @IBOutlet weak var emoji19: UIButton!
    @IBOutlet weak var emoji20: UIButton!
    @IBOutlet weak var emoji21: UIButton!
    
    var allEmojis: [UIButton] = []
    var allEmojiCharacters: [Emoji] = []
    
    var player1: Int? = nil
    var player2: Int? = nil

    override func viewDidLoad()
    {
        allEmojis.append(emoji1)
        allEmojis.append(emoji2)
        allEmojis.append(emoji3)
        allEmojis.append(emoji4)
        allEmojis.append(emoji5)
        allEmojis.append(emoji6)
        allEmojis.append(emoji7)
        allEmojis.append(emoji8)
        allEmojis.append(emoji9)
        allEmojis.append(emoji10)
        allEmojis.append(emoji11)
        allEmojis.append(emoji12)
        allEmojis.append(emoji13)
        allEmojis.append(emoji14)
        allEmojis.append(emoji15)
        allEmojis.append(emoji16)
        allEmojis.append(emoji17)
        allEmojis.append(emoji18)
        allEmojis.append(emoji19)
        allEmojis.append(emoji20)
        allEmojis.append(emoji21)
        
       loadCharacters()
    }
    
    func loadCharacters()
    {
        allEmojiCharacters.append(Emoji(filename: "Alien", nickname: "The Alien"))
        allEmojiCharacters.append(Emoji(filename: "Bear", nickname: "Big Bear"))
        allEmojiCharacters.append(Emoji(filename: "Cat", nickname: "Meow Cat"))
        allEmojiCharacters.append(Emoji(filename: "Cow", nickname: "Moo Cow"))
        allEmojiCharacters.append(Emoji(filename: "Dog", nickname: "Woof Dog"))
        allEmojiCharacters.append(Emoji(filename: "Robiger", nickname: "The Rabbit"))
        allEmojiCharacters.append(Emoji(filename: "Fox", nickname: "Master Fox"))
        allEmojiCharacters.append(Emoji(filename: "Chicken", nickname: "Poulet"))
        allEmojiCharacters.append(Emoji(filename: "Hamster", nickname: "Runner"))
        allEmojiCharacters.append(Emoji(filename: "Koala", nickname: "KBear"))
        allEmojiCharacters.append(Emoji(filename: "Lion", nickname: "Roar!"))
        allEmojiCharacters.append(Emoji(filename: "Monkey Girl", nickname: "La Singe"))
        allEmojiCharacters.append(Emoji(filename: "Monkey", nickname: "Le Singe"))
        allEmojiCharacters.append(Emoji(filename: "Mouse", nickname: "Jerry"))
        allEmojiCharacters.append(Emoji(filename: "Panda", nickname: "PBear"))
        allEmojiCharacters.append(Emoji(filename: "Pig", nickname: "Porky"))
        allEmojiCharacters.append(Emoji(filename: "Poop", nickname: "Poopy"))
        allEmojiCharacters.append(Emoji(filename: "Rabbit", nickname: "Bugs Bunny"))
        allEmojiCharacters.append(Emoji(filename: "Robot", nickname: "Robo"))
        allEmojiCharacters.append(Emoji(filename: "Tiger", nickname: "Tigger"))
        allEmojiCharacters.append(Emoji(filename: "Rocat", nickname: "Meow!"))
        
        for (i, currentEmoji) in allEmojis.enumerated()
        {
            let correspondingEmoji = allEmojiCharacters[i]
            
            currentEmoji.setImage(UIImage(named: correspondingEmoji.filename), for: .normal)
            currentEmoji.layer.cornerRadius = 15
        }
    }
    
    @IBAction func emojiTapped(_ sender: Any)
    {
        let emojiNo = (sender as! UIButton).tag - 1
        if player1 == nil
        {
            player1 = emojiNo
            allEmojis[emojiNo].backgroundColor = .blue
        }
        else
        {
            player2 = emojiNo
            allEmojis[emojiNo].backgroundColor = .red
            fadeOutUnusedEmojis()
            
            Timer.scheduledTimer(timeInterval: 2,
                                 target: self,
                                 selector: #selector(startGame),
                                 userInfo: nil,
                                 repeats: false)
        }
    }
    
    func fadeOutUnusedEmojis()
    {
        for (i, currentEmoji) in allEmojis.enumerated()
        {
            if i != player1 && i != player2
            {
                currentEmoji.alpha = 0.1
            }
        }
    }
    
    func fadeInUsedEmojis()
    {
        for (i, currentEmoji) in allEmojis.enumerated()
        {
            if i != player1 && i != player2
            {
                currentEmoji.alpha = 1
            }
        }
    }
    
    @objc func startGame()
    {
        let sb = UIStoryboard(name: "Game", bundle: Bundle.main)
        guard let vc = sb.instantiateViewController(withIdentifier: "GameVC") as? GameVC else { return }
        vc.modalTransitionStyle = .crossDissolve
        
        vc.player1 = player1
        vc.player2 = player2
        vc.allEmojiCharacters = allEmojiCharacters                              
        
        present(vc, animated: true, completion: nil)
    }
}


