import Foundation
import AVFoundation

class AudioHelpers
{
    class func getAudioPlayer(_ soundName: String, fileExtension: String = ".wav", rate: Float = 1.0, volume: Float = 1.0) -> AudioPlayerHolder?
    {
        let bundle = Bundle(for: self)
        let soundPath = bundle.path(forResource: soundName, ofType: fileExtension)!
        
        let player = try? AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundPath))
        
        if rate != 1.0
        {
            player?.enableRate = true
            player?.rate = rate
        }
        
        player?.volume = volume
        
        player?.prepareToPlay()
        return AudioPlayerHolder(player: player)
    }
}

class AudioPlayerHolder
{
    var player: AVAudioPlayer?
    
    init(player: AVAudioPlayer?)
    {
        self.player = player
    }
}
